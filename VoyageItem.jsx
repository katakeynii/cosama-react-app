
import $ from 'jquery';

import React from 'react';
import ReactDOM from 'react-dom';


export default class VoyageItem extends React.Component {
  constructor(props){
  	super(props);
  	this.state = {

  	}
  }
  openVoyage(e){

		e.preventDefault();
		var position = jQuery(e.target).data("position");
		var pos = jQuery(e.target).attr("data-position");
    var voyage = this.state.voyages[position];
    console.log(pos);
    console.log(jQuery(e.target));
    console.log(position);

  }
  render(){
		var date_depart = this.props.date_depart;
		var date_arrive = this.props.date_arrive;
    var voyage = this.props.voyage;

    return(
      <a
        className="voyage_item row"
        onClick={this.openVoyage.bind(this)}
        data-position={index}>
        <span className="col-md-12 voyage-infos">
          <span className="voyage_info">
            <i className=" voyage_icon fa fa-calendar-check-o"></i>
              {date_depart} - {date_arrive}
          </span>
          <span className="voyage_info">
            <i className=" voyage_icon fa fa-ship"></i>
              {voyage.batNom}
          </span>
          <span className="voyage_info">
            <i className=" voyage_icon fa fa-anchor"></i>
              {voyage.vilNom}
          </span>
        </span>

      </a>

  	)
  }
}
