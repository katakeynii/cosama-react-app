module.exports = {
	"departs":[
		{
			"datedpt": "Wed Jul 20 2016 12:23:59 GMT+0000 (GMT)",
			"datearriv": "Wed Jul 20 2016 12:23:59 GMT+0000 (GMT)",
			"depart": "Dakar",
			"destination": "Ziguinchor",
			"etat": "on",	
	    "bateau": {
	  		"nom":"Alioune Siteo",
	  		"reference": "AS",
	  		"description": "lorem ipsum descriptum",
	  		"nbplace": "300",
	  		"etat":"OK",
	  		"places":[
	  			{
	  				"code": "as-pl-1",
	  				"type": {
	  					"code": "aspltype1",
	  					"prix": "15000",
	  					"nom": "Cabine à 2"
	  				},
	  				"niveau":"nom niveau",
	  				"etat": true
	  			},
	  			{
	  				"code": "as-pl-2",
	  				"type": {
	  					"code": "aspltype2",
	  					"prix": "30000",
	  					"nom": "Cabine à 3"
	  				},
	  				"niveau":"",
	  				"etat": ""
	  			},
	  			{
	  				"code": "as-pl-3",
	  				"type": {
	  					"code": "aspltype3",
	  					"prix": "22500",
	  					"nom": "fauteuil"
	  				},
	  				"niveau":"nom niveau",
	  				"etat": true
	  			},
	  			{
	  				"code": "as-pl-4",
	  				"type": {
	  					"code": "aspltype4",
	  					"prix": "12000",
	  					"nom": "Cabine à 8"
	  				},
	  				"niveau":"",
	  				"etat": false
	  			} 			  			  			  			  			  			
	  		]
	  	}
    },
		{
			"datedpt": "Wed Jul 27 2016 12:23:59 GMT+0000 (GMT)",
			"datearriv": "Wed Jul 29 2016 12:23:59 GMT+0000 (GMT)",
			"depart": "Ziguinchor",
			"destination": "Dakar",
			"etat": "on",	
	    "bateau": {
	  		"nom":"Alioune Siteo",
	  		"reference": "AS",
	  		"description": "lorem ipsum descriptum",
	  		"nbplace": "300",
	  		"etat":"OK",
	  		"places":[
	  			{
	  				"code": "as-pl-1",
	  				"type": {
	  					"code": "aspltype1",
	  					"prix": "15000",
	  					"nom": "Cabine à 2"
	  				},
	  				"niveau":"nom niveau",
	  				"etat": false
	  			},
	  			{
	  				"code": "as-pl-2",
	  				"type": {
	  					"code": "aspltype2",
	  					"prix": "30000",
	  					"nom": "Cabine à 3"
	  				},
	  				"niveau":"",
	  				"etat": false
	  			},
	  			{
	  				"code": "as-pl-3",
	  				"type": {
	  					"code": "aspltype3",
	  					"prix": "22500",
	  					"nom": "fauteuil"
	  				},
	  				"niveau":"nom niveau",
	  				"etat": true
	  			},
	  			{
	  				"code": "as-pl-4",
	  				"type": {
	  					"code": "aspltype4",
	  					"prix": "12000",
	  					"nom": "Cabine à 8"
	  				},
	  				"niveau":"",
	  				"etat": false
	  			} 			  			  			  			  			  			
	  		]
	  	}
    },
		{
			"datedpt": "Wed Aug 01 2016 12:23:59 GMT+0000 (GMT)",
			"datearriv": "Wed Aug 02 2016 12:23:59 GMT+0000 (GMT)",
			"depart": "Dakar",
			"destination": "Carabane",
			"etat": "on",	
	    "bateau": {
	  		"nom":"Alioune Siteo",
	  		"reference": "AS",
	  		"description": "lorem ipsum descriptum",
	  		"nbplace": "300",
	  		"etat":"OK",
	  		"places":[
	  			{
	  				"code": "as-pl-1",
	  				"type": {
	  					"code": "aspltype1",
	  					"prix": "15000",
	  					"nom": "Cabine à 2"
	  				},
	  				"niveau":"nom niveau",
	  				"etat": true
	  			},
	  			{
	  				"code": "as-pl-2",
	  				"type": {
	  					"code": "aspltype2",
	  					"prix": "30000",
	  					"nom": "Cabine à 3"
	  				},
	  				"niveau":"",
	  				"etat": true
	  			},
	  			{
	  				"code": "as-pl-3",
	  				"type": {
	  					"code": "aspltype3",
	  					"prix": "22500",
	  					"nom": "fauteuil"
	  				},
	  				"niveau":"nom niveau",
	  				"etat": false
	  			},
	  			{
	  				"code": "as-pl-4",
	  				"type": {
	  					"code": "aspltype4",
	  					"prix": "12000",
	  					"nom": "Cabine à 8"
	  				},
	  				"niveau":"",
	  				"etat": false
	  			} 			  			  			  			  			  			
	  		]
	  	}
    },        
		{
			"datedpt": "Wed Aug 10 2016 12:23:59 GMT+0000 (GMT)",
			"datearriv": "Wed Aug 12 2016 12:23:59 GMT+0000 (GMT)",
			"depart": "Dakar",
			"destination": "Ziguinchor",
			"etat": "on",	
	    "bateau": {
	  		"nom":"Alioune Siteo",
	  		"reference": "AS",
	  		"description": "lorem ipsum descriptum",
	  		"nbplace": "300",
	  		"etat":"OK",
	  		"places":[
	  			{
	  				"code": "as-pl-1",
	  				"type": {
	  					"code": "aspltype1",
	  					"prix": "15000",
	  					"nom": "Cabine à 2"
	  				},
	  				"niveau":"nom niveau",
	  				"etat": false
	  			},
	  			{
	  				"code": "as-pl-2",
	  				"type": {
	  					"code": "aspltype2",
	  					"prix": "30000",
	  					"nom": "Cabine à 3"
	  				},
	  				"niveau":"",
	  				"etat": false
	  			},
	  			{
	  				"code": "as-pl-3",
	  				"type": {
	  					"code": "aspltype3",
	  					"prix": "22500",
	  					"nom": "fauteuil"
	  				},
	  				"niveau":"nom niveau",
	  				"etat": false
	  			},
	  			{
	  				"code": "as-pl-4",
	  				"type": {
	  					"code": "aspltype4",
	  					"prix": "12000",
	  					"nom": "Cabine à 8"
	  				},
	  				"niveau":"",
	  				"etat": false
	  			} 			  			  			  			  			  			
	  		]
	  	}
    }
	]
}