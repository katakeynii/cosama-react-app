import React from 'react';
import ReactDOM from 'react-dom';
import VoyagesList from './src/components/VoyagesList';
import VoyageItem from './public/VoyageItem';

// require("./src/css/bootstrap-datetimepicker.min.css");
// require("./src/js/bootstrap-datetimepicker.min.js");
//require("./src/css/design.css");
let DATAS =require("./src/datas.js");
let SERVER_URL = "http://196.207.209.209:2008";
let ACCESS_KEY = "089863b5ae158389b533961288b3044816643070"

export class App extends React.Component {
	constructor(props){
		super(props);
		this.state={
			voyages: [],
			bateaux: [],
			villes: [],
			places: [],
			cabine2: [],
			cabine4: [],
			cabine8: [],
			chaise: [],
			placeClassifie: [],
			show_places: false
		}
	}
	openVoyage(e){
    e.preventDefault();
    var $target  = jQuery(e.target);
    var position;
    if( $target.hasClass("voyage_info") ){
       position =  $target.parent().parent().data('position');
    }else{
      position =  $target.data('position');
    }
    var voyage = this.state.voyages[position];
    var date = voyage.voyDatedpt.split("T")[0];
    this.fetchPlaces(date, voyage.batNom, voyage.vilNom);
 }
	formatDate(date){
		var obj = new Date(date);
		return obj.toLocaleDateString();
	}
	makeVoyageItem(voyage, index){
		var date_depart = this.formatDate(voyage.voyDatedpt);
		var date_arrive = this.formatDate(voyage.voyDatearriv);

    return (
      <a key={index}
        className="voyage_item row"
        onClick={this.openVoyage.bind(this)}
        data-position={index}>
        <span className="col-md-12 voyage-infos">
          <span className="voyage_info">
            <i className=" voyage_icon fa fa-calendar-check-o"></i>
              {date_depart} - {date_arrive}
          </span>
          <span className="voyage_info">
            <i className=" voyage_icon fa fa-ship"></i>
              {voyage.batNom}
          </span>
          <span className="voyage_info">
            <i className=" voyage_icon fa fa-anchor"></i>
              {voyage.vilNom}
          </span>
        </span>

      </a>

   )
	}

	makeBateauOptions(item, i){
		return(
			<option key={i} value={item.bat_id} >
				{item.bat_nom}
			</option>
		);
	}
	makeVilleOptions(item, i){
		return(
			<option key={i} value={item.vil_id} >
				{item.vil_nom}
			</option>
		);
	}
	filterLibrePlace(place){
		return place.etat == true;
	}
	updateData(){

	}
  componentDidMount(){
    $.fn.datepicker.dates['fr'] = {
          days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
          daysShort: ["Dim","Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
          daysMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
          months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"],
          monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
          today: "Aujourd'hui",
          clear: "Clear",
          format: "mm/dd/yyyy",
          titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
          weekStart: 0
    };

    $('.datepicker').datepicker({
      language: "fr",
      templates: {
       leftArrow: '<i class="fa navigation-calendar fa-caret-left"></i>',
       rightArrow: '<i class="fa navigation-calendar fa-caret-right"></i>'
       },
    });

		var url = SERVER_URL+"/api/index?key="+ACCESS_KEY;
		jQuery.getJSON(url,
			function(data){
        jQuery("#loaders").hide();
				this.setState({
					bateaux: data.bateaux,
					voyages: data.voyages,
					villes: data.villes
        });
      jQuery("#loaders").hide();
			}.bind(this));
	}
    convert_date(date) {
        var year, month, day;
		var t = date.split("/");
		var d = new Date(t[2], t[0], t[1]);
        year = String(d.getFullYear());
        month = String(d.getMonth());
        if (month.length == 1) {
            month = "0" + month;
        }
        day = String(d.getDate());
        if (day.length == 1) {
            day = "0" + day;
        }
        var result = t[2] + "-" + t[0] + "-" + t[1];
        return result;
    }
  fetchPlaces(mysDate, bateau, ville){
    console.log(mysDate);
     var url = SERVER_URL+"/api/places/libres?key="+ACCESS_KEY;
    jQuery("#loaders").show();
		jQuery.getJSON(url,
		{
			date: mysDate,
			bateau: bateau,
			ville : ville
		},
		function(data){
			var show_places = true;
			var chaise = data.places.filter(function(item){return item.tplCode == "CH";});
			var cabine2 = data.places.filter(function(item){return item.tplCode == "C2";});
			var cabine4 = data.places.filter(function(item){return item.tplCode == "C4";});
			var cabine8 = data.places.filter(function(item){return item.tplCode == "C8";});

			var placeClassifie = {ch: chaise, c2: cabine2, c4: cabine4, c8: cabine8};
			if(data.has_voyage == false){
        jQuery("#place-content").html("<h1 class='alert-info'>Il n'y pas de voyage programmé à cette date</h1>");
				show_places = false;
			}
			if(data.bateau_full == true){
        jQuery("#place-content").html("<h1 class='alert-info' >Il n'y pas de voyage programmé à cette date</h1>");
				show_places= false;
			}
			this.setState({
				places: data.places,
				placeClassifie: placeClassifie,
				show_places: show_places,
				chaise: chaise,
				cabine8: cabine8,
				cabine4: cabine4,
				cabine2: cabine2
      });

      jQuery("#loaders").hide();

		}.bind(this))

  }
  submitForm(e){
		e.preventDefault();
		var date = jQuery("#date-text").val();
		var mysDate = this.convert_date(date);

		var bateau = jQuery("#bateau-select").val();
    var ville = jQuery("#ville-select").val();
    this.fetchPlaces(mysDate, bateau, ville);
	}
	filterCabineType(cabines){
		var results = cabines.reduce(function(result, cabine, index){
			if(cabine.sexe == ""){
				result.mixte += 1;
			}else{
				if(cabine.sexe == "H"){
					result.homme += 1;
				}
				if(cabine.sexe == "F"){
					result.femme += 1;
				}
			}
			return result;
		}, {mixte: 0, homme:0, femme:0});
		return results;
	}
	renderPlaces(){
		let cabine2 = this.filterCabineType(this.state.cabine2);
		let cabine4 = this.filterCabineType(this.state.cabine4);
		let cabine8 = this.filterCabineType(this.state.cabine8);
		return(
			<div className="places_container">
			    <div  className="row" id="chaise">
			    	<h5> {this.state.chaise.length} Chaises </h5>
			    </div>
			    <div  className="result-row " id="cabine2">
			    	<h5  className="cabine-title"> Cabine de 2 <span className="nb_places_libres">({this.state.cabine2.length} places libres ) </span></h5>
			    	<div className="gender" >
			    		<i className="fa fa-venus-mars"></i> Mixte { (typeof cabine2 != 'undefined') ? cabine2.mixte : "0"}
			    	</div>
			    	<div className="gender" >
			    		<i className="fa fa-venus"></i> Homme { (typeof cabine2 != 'undefined') ? cabine2.homme : "0"}
			    	</div>
			    	<div className="gender" >
			    		<i className="fa fa-mars"></i> Femme { (typeof cabine2 != 'undefined') ? cabine2.femme : "0"}
			    	</div>

			    </div>
			    <div  className="result-row" id="cabine4">
			    	<h5 className="cabine-title"> Cabine de 4 <span className="nb_places_libres">({this.state.cabine4.length} places libres)  </span></h5>

			    	<div className="gender" >
			    		<i className="fa fa-venus-mars"></i> Mixte { (typeof cabine4 != 'undefined') ? cabine4.mixte : "0"}
			    	</div>

			    	<div className="gender" >
			    		<i className="fa fa-venus"></i> Homme { (typeof cabine4 != 'undefined') ? cabine4.homme : "0"}
			    	</div>

			    	<div className="gender" >
			    		<i className="fa fa-mars"></i> Femme { (typeof cabine4 != 'undefined') ? cabine4.femme : "0"}
			    	</div>
			    </div>
			    <div  className="result-row " id="cabine8">
			    	<h5 className="cabine-title"> Cabine de 8 <span className="nb_places_libres">({this.state.cabine8.length} places libres) </span></h5>
			    	<div className="gender" >
			    		<i className="fa fa-venus-mars"></i> Mixte { (typeof cabine8 != 'undefined') ? cabine8.mixte : "0"}
			    	</div>

			    	<div className="gender" >
			    		<i className="fa fa-venus"></i> Homme { (typeof cabine8 != 'undefined') ? cabine8.homme : "0"}
			    	</div>

			    	<div className="gender" >
			    		<i className="fa fa-mars"></i> Femme { (typeof cabine8 != 'undefined') ? cabine8.femme : "0"}
			    	</div>
			    </div>
			</div>
		)
	}

	render() {
		var placeLibreList = "Pas de places libre pour ce voyage";

		var bateauOptions = this.state.bateaux.map(this.makeBateauOptions.bind(this));
		var villesOptions = this.state.villes.map(this.makeVilleOptions.bind(this));
		var voyageList = this.state.voyages.map(this.makeVoyageItem.bind(this));
		let content = this.state.show_places == true ? this.renderPlaces() : voyageList;

		return (
            <div >
            	<div className="" id="header" >
		            <div className="form-section date-container ">
						<div className="input-group date" >
						    <input type="text" className=" datepicker form-control form-item"
						    		id="date-text"
						    		placeholder="Choisir la date de voyage" />
						</div>
					</div>
		            <div className="form-section ">
		            	<select className="form-control form-item" id="bateau-select">
		            		<option> Choisir Bateau</option>
		            		{bateauOptions}
		            	</select>
		            </div>
		            <div className="form-section">
		            	<select className="form-control form-item" id="ville-select">
		            		<option> Port d'embarquement </option>
		            		{villesOptions}
		            	</select>
		            </div>
		            <div className= "btn-search">
		            	<a href="#" onClick={this.submitForm.bind(this)} className="btn btn-primary flat-btn">
		            		<i className="fa fa-search" ></i>
		            	</a>
		            </div>

            	</div>
              <div className="col-md-12">
                <div id="loaders">
                  <img src="/ring.gif"/>
                </div>
                <div id="place-content">
	            	  { content }
                </div>
	            </div>
			</div>
		);
	}
}

ReactDOM.render(<App/>, document.querySelector("#myApp"));
